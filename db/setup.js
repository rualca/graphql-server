const { Model } = require('objection');
const knex = require('knex');
const knexConfig = require('./knexfile');

const knexConnection = knex(knexConfig.development);
Model.knex(knexConnection);
