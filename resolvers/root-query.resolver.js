const courseResolver = require('./course.resolver');
const teacherResolver = require('./teacher.resolver');
const commentResolver = require('./comment.resolver');
const commonResolver = require('./common.resolver');

module.exports = {
  courses: courseResolver.getAllCourses,
  course: courseResolver.getCourseById,
  teachers: teacherResolver.getAllTeachers,
  teacher: teacherResolver.getTeacherById,
  comments: commentResolver,
  find: commonResolver.find
};
