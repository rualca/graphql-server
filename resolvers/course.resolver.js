const Course = require('../models/course.model');

function getAllCourses() {
  return Course.query().eager('[teacher, comments]');
}

function getCourseById(rootValue, args) {
  return Course.query().findById(args.id).eager('[teacher, comments]');
}

module.exports = {
  getAllCourses,
  getCourseById
};
