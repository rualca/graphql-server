const rootQueryResolver = require('./root-query.resolver');
const mutationResolver = require('./mutation.resolver');

module.exports = {
  rootQueryResolver,
  mutationResolver
};
