const {addTeacher, editTeacher} = require('./teacher.resolver');

module.exports = {
  addTeacher,
  editTeacher
};
