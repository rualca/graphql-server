const Teacher = require('../models/teacher.model');

function getAllTeachers() {
  return Teacher.query().eager('courses');
}

function getTeacherById(rootValue, args) {
  return Teacher.query().findById(args.id).eager('courses');
}

function addTeacher(_, args) {
  return Teacher.query().insert(args.teacher);
}

function editTeacher(_, args) {
  return Teacher.query().patchAndFetchById(args.teacherId, args.teacher);
}

function deleteTeacher(_, args) {
  return Teacher.query().findById(args.teacherId).then(teacher => {
    return Teacher.query().deleteById(args.teacherId).then(() => teacher);
  });
}

module.exports = {
  getAllTeachers,
  getTeacherById,
  addTeacher,
  editTeacher,
  deleteTeacher
};
