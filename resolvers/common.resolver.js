const Course = require('../models/course.model');
const Teacher = require('../models/teacher.model');

function find(_, args) {
  return [
    Course.query().findById(1),
    Teacher.query().findById(1)
  ];
}

module.exports = {
  find
};
