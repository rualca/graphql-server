const Comment = require('../models/comment.model');

function commentResolver() {
  return Comment.query();
}

module.exports = commentResolver;
