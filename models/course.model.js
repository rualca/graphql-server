const path = require('path');
const { Model } = require('objection');

class Course extends Model {
  static get tableName() {
    return 'courses';
  }

  static get relationMappings() {
    return {
      teacher: {
        relation: Model.BelongsToOneRelation,
        modelClass: path.join(__dirname, '/teacher.model'),
        join: {
          from: 'courses.teacher_id',
          to: 'teachers.id'
        }
      },
      comment: {
        relation: Model.HasManyRelation,
        modelClass: path.join(__dirname, '/comment.model'),
        join: {
          from: 'courses.id',
          to: 'comments.course_id'
        }
      }
    };
  }
}

module.exports = Course;
