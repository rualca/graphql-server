const path = require('path');
const { Model } = require('objection');

class Teacher extends Model {
  static get tableName() {
    return 'teachers';
  }

  static get relationMappings() {
    return {
      courses: {
        relation: Model.HasManyRelation,
        modelClass: path.join(__dirname, '/course.model'),
        join: {
          from: 'teachers.id',
          to: 'courses.teacher_id'
        }
      }
    };
  }
}

module.exports = Teacher;
