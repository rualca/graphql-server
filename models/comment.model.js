const path = require('path');
const { Model } = require('objection');

class Comment extends Model {
  static get tableName() {
    return 'comments';
  }

  static get relationMappings() {
    return {
      course: {
        relation: Model.BelongsToOneRelation,
        modelClass: path.join(__dirname, '/course.model'),
        join: {
          from: 'comments.curso_id',
          to: 'courses.id'
        }
      }
    };
  }
}

module.exports = Comment;
