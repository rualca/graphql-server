const commentSchema = `
  type Comment {
    id: ID!
    name: String!,
    body: String!
  }
`;

module.exports = commentSchema;
