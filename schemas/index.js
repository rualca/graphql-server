const { makeExecutableSchema } = require('graphql-tools');
const { rootQueryResolver, mutationResolver } = require('../resolvers');
const courseSchema = require('./course.schema');
const teacherSchema = require('./teacher.schema');
const commentSchema = require('./comment.schema');
const rootQuerySchema = require('./root-query.schema');
const mutationSchema = require('./mutation.schema');

const schema = makeExecutableSchema({
  typeDefs: [
    rootQuerySchema,
    mutationSchema,
    courseSchema,
    teacherSchema,
    commentSchema
  ],
  resolvers: {
    Query: rootQueryResolver,
    Mutation: mutationResolver,
    FindResult: {
      __resolveType: (obj) => {
        if (obj.name) {
          return 'Teacher';
        }
        return 'Course';
      }
    }
  }
});

module.exports = schema;
