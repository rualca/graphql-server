const rootQuerySchema = `
  union FindResult = Course | Teacher

  type Query {
    courses: [Course],
    teachers: [Teacher],
    comments: [Comment]
    course(id: Int): Course,
    teacher(id: Int): Teacher,
    find(query: String!): [FindResult]
  }
`;

module.exports = rootQuerySchema;
