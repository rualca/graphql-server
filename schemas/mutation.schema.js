const mutationSchema = `
  type Mutation {
    addTeacher(teacher: newTeacher): Teacher,
    editTeacher(teacherId: Int!, teacher: TeacherEditable): Teacher,
    deleteTeacher(teacherId: Int): Teacher
  }
`;

module.exports = mutationSchema;
