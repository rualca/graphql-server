const courseSchema = `
  type Course {
    id: ID!
    title: String!,
    description: String!
    teacher: Teacher!,
    rating: Float,
    comments: [Comment],
    prove_field: String @deprecated(reaseon: "A deprecated field")
  }
`;

module.exports = courseSchema;
