const teacherSchema = `
  type Teacher {
    id: ID!
    name: String!,
    country: String!
    teacher: Teacher,
    rating: Float,
    courses: [Course]
  }

  input newTeacher {
    name: String!,
    country: String!
  }

  input TeacherEditable {
    name: String,
    country: String
  }
`;

module.exports = teacherSchema;
