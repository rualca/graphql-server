const express = require('express');
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express');
const schema = require('./schemas');
require('./db/setup');

const app = express();
const PORT = 8000;

app.use(express.urlencoded({extended: true}));
app.use(express.json());

app.use('/graphql', graphqlExpress({
  schema
}));

app.use('/graphiql', graphiqlExpress({
  endpointURL: '/graphql'
}));

app.listen(PORT, () => {
  console.log(`Server is running in localhost:${PORT}`);
});
